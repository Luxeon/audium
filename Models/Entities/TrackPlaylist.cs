﻿namespace Audium.Models.Entities
{
    public class TrackPlaylist
    {
        public int TrackId { get; set; }

        public Track Track { get; set; }

        public int PlaylistId { get; set; }

        public Playlist Playlist { get; set; }

        public TrackPlaylist()
        {

        }

        public TrackPlaylist(Track track, Playlist playlist)
        {
            Track = track;
            Playlist = playlist;
        }
    }
}
