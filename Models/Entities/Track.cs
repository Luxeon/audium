﻿using System.Collections.Generic;

namespace Audium.Models.Entities
{
    public class Track
    {
        public int TrackId { get; set; }

        public string Name { get; set; }

        public string Path { get; set; }

        public string User { get; set; }

        public IList<TrackPlaylist> TrackPlaylists { get; set; }
    }
}
