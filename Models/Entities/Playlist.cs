﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Audium.Models.Entities
{
    public class Playlist
    {
        public int PlaylistId { get; set; }

        [Required]
        public string Name { get; set; }

        public string User { get; set; }

        public IList<TrackPlaylist> TrackPlaylists { get; set; }
    }
}
