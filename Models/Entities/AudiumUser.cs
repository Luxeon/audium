﻿using Microsoft.AspNetCore.Identity;

namespace Audium.Models.Entities
{
    public class AudiumUser : IdentityUser
    {
        public bool DarkMode { get; set; }
    }
}
