﻿using Audium.Models.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Audium.Models.DbContexts
{
    public class IdentityDbContext : IdentityDbContext<AudiumUser>
    {
        public IdentityDbContext() : base()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=IdentityDb.db");
        }
    }
}
