﻿using Audium.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace Audium.Models.DbContexts
{
    public class MusicDbContext : DbContext
    {
        public DbSet<Playlist> Playlists { get; set; }
        public DbSet<Track> Tracks { get; set; }

        public MusicDbContext() : base()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=MusicDb.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TrackPlaylist>()
                .HasKey(tp => new { tp.TrackId, tp.PlaylistId });

            modelBuilder.Entity<TrackPlaylist>()
                .HasOne(tp => tp.Track)
                .WithMany(t => t.TrackPlaylists)
                .HasForeignKey(tp => tp.TrackId);

            modelBuilder.Entity<TrackPlaylist>()
                .HasOne(tp => tp.Playlist)
                .WithMany(p => p.TrackPlaylists)
                .HasForeignKey(tp => tp.PlaylistId);
        }
    }
}
