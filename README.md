Audium is a web application to create and play playlists with your own audio files.

This is an individual project carried out in **ASP.NET** during my 3rd year of study. The project is available for testing purpose on my portfolio just right [here][1].

*Warning!* This is a demonstration of skills, the tool should not be used for storage purposes. In fact, the app is reset every day and your data will be deleted at the end of each day.

How to use it ?
---------------

* In order to use Audium, it is necessary to create an account by clicking on *Create an account*
* Once connected, you can create an empty playlist using the left menu
* By clicking on it, it is possible to add one or more musics. For this, there are 2 possibilities:
     * either files have already been uploaded for other playlists and it is possible to reuse them
     * or it is possible to search for files directly on your computer
* It is then possible to play the playlist or each track individually
* For each element (playlist or track), it is of course possible to modify/delete them
* *Little extra*: a dark mode is available on the application

Installation
------------

It is possible for you to test the application by yourself locally. For this, the source files are at your disposal.

You also have a `docker-compose.yml` file allowing you to launch the application in a Docker container on port 8080.

About me
--------

I am a Computer Science Master student. You can find some of my other projects on my [Portfolio][2]. 

[1]: https://audium.cyrilpiejougeac.dev
[2]: https://portfolio.cyrilpiejougeac.dev
