﻿using Audium.Models.DbContexts;
using Audium.Models.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Audium.Controllers
{
    [Authorize]
    public class PlaylistController : Controller
    {
        private readonly MusicDbContext _musicDb;
        private readonly UserManager<AudiumUser> _userManager;
        private readonly IHostingEnvironment _env;

        public PlaylistController(MusicDbContext musicDb, UserManager<AudiumUser> userManager, IHostingEnvironment env)
        {
            _musicDb = musicDb;
            _userManager = userManager;
            _env = env;
        }

        [Route("/playlist/{id:int}")]
        public IActionResult Show(int id)
        {
            var playlist = _musicDb.Playlists
                .Where(p => p.User == _userManager.GetUserId(User))
                .Where(p => p.PlaylistId == id)
                .Include(p => p.TrackPlaylists)
                .ThenInclude(tp => tp.Track)
                .FirstOrDefault();

            if (playlist == null)
            {
                return RedirectToAction("Index", "Home");
            }

            if (playlist.TrackPlaylists.Count == 0)
            {
                return RedirectToAction("Add", "Track", new { id = id });
            }

            return View(playlist);
        }

        [Route("/playlist/new")]
        public IActionResult New()
        {
            return View(new Playlist());
        }

        [HttpPost]
        [Route("/playlist/new")]
        public IActionResult New(Playlist playlist)
        {
            if (!ModelState.IsValid)
            {
                return View(playlist);
            }

            playlist.TrackPlaylists = new List<TrackPlaylist>();
            playlist.User = _userManager.GetUserId(User);

            _musicDb.Playlists.Add(playlist);

            _musicDb.SaveChanges();

            return RedirectToAction("Index", "Home");
        }

        [Route("/playlist/{id}/edit")]
        public IActionResult Edit(int id)
        {
            Playlist playlist = _musicDb.Playlists
                .Where(p => p.User == _userManager.GetUserId(User))
                .Where(p => p.PlaylistId == id)
                .FirstOrDefault();

            return View(playlist);
        }

        [HttpPost]
        [Route("/playlist/{id}/edit")]
        public IActionResult Edit(Playlist playlist, int id)
        {
            if (!ModelState.IsValid)
            {
                return View(playlist);
            }

            _musicDb.Playlists
                .Where(p => p.User == _userManager.GetUserId(User))
                .Where(p => p.PlaylistId == id)
                .FirstOrDefault()
                .Name = playlist.Name;

            _musicDb.SaveChanges();

            return RedirectToAction("Show", "Playlist", new { id = id });
        }



        [HttpPost]
        [Route("/playlist/{id}/delete")]
        public IActionResult Delete(int id)
        {
            Playlist playlist = _musicDb.Playlists
                .Where(p => p.User == _userManager.GetUserId(User))
                .Where(p => p.PlaylistId == id)
                .FirstOrDefault();

            _musicDb.Remove(playlist);

            _musicDb.SaveChanges();

            return RedirectToAction("Index", "Home");
        }
    }
}