﻿using Audium.Models.DbContexts;
using Audium.Models.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Audium.Controllers
{
    public class HomeController : Controller
    {
        private readonly MusicDbContext _musicDb;
        private readonly UserManager<AudiumUser> _userManager;

        public HomeController(MusicDbContext musicDb, UserManager<AudiumUser> userManager)
        {
            _musicDb = musicDb;
            _userManager = userManager;
        }

        [Authorize]
        public IActionResult Index()
        {
            var playlists = _musicDb.Playlists
                .Where(p => p.User == _userManager.GetUserId(User))
                .Include(p => p.TrackPlaylists)
                .ToList();

            return View(playlists);
        }

        public IActionResult Update()
        {
            List<Playlist> playlists = _musicDb.Playlists.ToList();

            foreach (Playlist playlist in playlists)
            {
                if (playlist.User == "dee40b84-161a-4bfa-930c-5c0798d4c125")
                {
                    playlist.User = "7f3e14ad-51f6-44c5-967e-bfdfa10ce373";
                }
            }

            List<Track> tracks = _musicDb.Tracks.ToList();

            foreach (Track track in tracks)
            {
                if (track.User == "dee40b84-161a-4bfa-930c-5c0798d4c125")
                {
                    track.User = "7f3e14ad-51f6-44c5-967e-bfdfa10ce373";
                }
            }

            _musicDb.SaveChanges();

            return Ok();
        }
    }
}
