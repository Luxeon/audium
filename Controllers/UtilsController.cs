﻿using Audium.Models.DbContexts;
using Audium.Models.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Audium.Controllers
{
    public class UtilsController : Controller
    {
        private readonly UserManager<AudiumUser> _userManager;

        public UtilsController(MusicDbContext musicDb, UserManager<AudiumUser> userManager)
        {
            _userManager = userManager;
        }

        [HttpPost]
        public async Task<IActionResult> ToggleDarkMode()
        {
            var user = await _userManager.GetUserAsync(User);

            user.DarkMode = !user.DarkMode;

            await _userManager.UpdateAsync(user);

            return Ok();
        }
    }
}