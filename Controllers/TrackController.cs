﻿using Audium.Models.DbContexts;
using Audium.Models.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Audium.Controllers
{
    [Authorize]
    public class TrackController : Controller
    {
        private readonly MusicDbContext _musicDb;
        private readonly IHostingEnvironment _env;
        private readonly UserManager<AudiumUser> _userManager;

        public TrackController(MusicDbContext musicDb, IHostingEnvironment env, UserManager<AudiumUser> userManager)
        {
            _musicDb = musicDb;
            _env = env;
            _userManager = userManager;
        }

        [Route("/playlist/{id:int}/add_tracks")]
        public IActionResult Add(int id)
        {
            ViewBag.Playlist = _musicDb.Playlists
                .Where(p => p.PlaylistId == id)
                .FirstOrDefault();

            ViewBag.ExistingTracks = _musicDb.Tracks
                .Where(t => t.User == _userManager.GetUserId(User))
                .ToList();

            return View(new Track());
        }

        [HttpPost]
        [Route("/playlist/{id:int}/add_tracks")]
        public async Task<IActionResult> AddOnPost(int id)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var playlist = _musicDb.Playlists
                .Where(p => p.User == _userManager.GetUserId(User))
                .Where(p => p.PlaylistId == id)
                .Include(p => p.TrackPlaylists)
                .FirstOrDefault();

            if (Request.Form["existingTracks"].Count > 0)
            {
                foreach (var trackId in Request.Form["existingTracks"])
                {
                    Track track = _musicDb.Tracks
                        .Where(t => t.TrackId == int.Parse(trackId))
                        .FirstOrDefault();

                    playlist.TrackPlaylists.Add(new TrackPlaylist(track, playlist));
                }
            }

            if (Request.Form.Files.Count > 0)
            {
                var files = Request.Form.Files;

                foreach (var file in files)
                {
                    if (file.ContentType.Contains("audio/"))
                    {
                        string newFileName = Path.GetRandomFileName().Split(".")[0] + Path.GetExtension(file.FileName);
                        string fullPath = Path.Combine(_env.WebRootPath, "uploads", newFileName);
                        string relativePath = "/uploads/" + newFileName;

                        Track track = new Track();

                        track.Name = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Path.GetFileNameWithoutExtension(file.FileName));
                        track.Path = relativePath;
                        track.User = _userManager.GetUserId(User);

                        _musicDb.Tracks.Add(track);

                        playlist.TrackPlaylists.Add(new TrackPlaylist(track, playlist));

                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                            await file.CopyToAsync(stream);
                        }
                    }
                }
            }

            _musicDb.SaveChanges();

            return RedirectToAction("Show", "Playlist", new { id = id });
        }

        [HttpPost]
        [Route("/playlist/{playlistId:int}/tracks/{trackId:int}/delete")]
        public IActionResult Delete(int playlistId, int trackId)
        {
            Playlist playlist = _musicDb.Playlists
                .Where(p => p.User == _userManager.GetUserId(User))
                .Where(p => p.PlaylistId == playlistId)
                .Include(p => p.TrackPlaylists)
                .FirstOrDefault();

            TrackPlaylist trackPlaylist = playlist.TrackPlaylists
                .Where(tp => tp.TrackId == trackId && tp.PlaylistId == playlistId)
                .FirstOrDefault();

            playlist.TrackPlaylists.Remove(trackPlaylist);

            _musicDb.SaveChanges();

            return RedirectToAction("Show", "Playlist", new { id = playlistId });
        }
    }
}