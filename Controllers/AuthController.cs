﻿using Audium.Models.Entities;
using Audium.Models.ViewModels.Auth;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Audium.Controllers
{
    public class AuthController : Controller
    {
        private readonly UserManager<AudiumUser> _userManager;
        private readonly SignInManager<AudiumUser> _signInManager;

        public AuthController(UserManager<AudiumUser> userManager, SignInManager<AudiumUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [Route("/register")]
        public IActionResult Register()
        {
            return View(new RegisterViewModel());
        }

        [HttpPost]
        [Route("/register")]
        public async Task<IActionResult> Register(RegisterViewModel registerFormInputs)
        {
            if (!ModelState.IsValid)
            {
                return View(registerFormInputs);
            }

            var user = new AudiumUser
            {
                Email = registerFormInputs.Email,
                UserName = registerFormInputs.Email
            };

            var result = await _userManager.CreateAsync(user, registerFormInputs.Password);

            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    string key = "";

                    if (error.Code.Contains("Password"))
                    {
                        key = "Password";
                    }
                    else if (error.Code.Contains("Email"))
                    {
                        key = "Email";
                    }

                    ModelState.AddModelError(key, error.Description);
                }

                return View();
            }

            return RedirectToAction("Login", "Auth");
        }

        [Route("/login")]
        public IActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(new LoginViewModel());
        }

        [HttpPost]
        [Route("/login")]
        public async Task<IActionResult> Login(LoginViewModel loginFormInputs, string returnUrl = null)
        {
            if (!ModelState.IsValid)
            {
                return View(loginFormInputs);
            }

            var result = await _signInManager.PasswordSignInAsync(loginFormInputs.Email, loginFormInputs.Password, loginFormInputs.RememberMe, false);

            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Invalid credentials");
                return View();
            }

            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                return RedirectToAction("Index", "Home");
            }

            return Redirect(returnUrl);
        }

        [HttpPost]
        [Route("/logout")]
        public async Task<IActionResult> Logout(string returnUrl = null)
        {
            await _signInManager.SignOutAsync();

            return RedirectToAction("Login", "Auth");
        }
    }
}